getAllLaunches();
var displayedLaunches = [];
var allLaunches
function getAllLaunches() {
    fetch("https://api.spacexdata.com/v3/launches").then(resp => {
        return resp.json();
    }).then(json => {
        allLaunches = json;
    }).then(function() {
        allLaunches.forEach(el => {
            displayedLaunches.push(el);
        });
        display();
    })
        
}
function display() {
    displayedLaunches.forEach(el => {
        let flightNumber = document.createElement("p");
        flightNumber.innerHTML = `Flight number: ${el.flight_number}`;
        let date = document.createElement("p");
        date.innerHTML = `Date of the launch: ${el.launch_date_utc}`;
        let name = document.createElement("p");
        name.innerHTML = `Rocket name: ${el.rocket.rocket_name}`;
        let status = document.createElement("p");
        status.innerHTML = `Status: ${el.upcoming ? 'upcoming':'launched'}`;
        let success = document.createElement("p");
        success.classList.add(successClass());
        success.innerHTML = `${successText()}`
        function successText() {
            if (el.upcoming) {
                return 'The rocket was not launched yet';
            } else if (!el.launch_success && !el.launch_upcoming) {
                return 'The launch was a failure';
            } else if (el.launch_success) {
                return 'The launch was successful';
            } else return 'No data';
        }
        function successClass() {
            if (el.launch_success) {
                return 'green';
            } else if (!el.launch_success && !el.launch_upcoming) {
                return 'red';
            } else if (!el.launch_success) {
                return 'yellow';
            }
        }
        let button = document.createElement("button");
        button.innerHTML = `More information`;
        let card = document.createElement("div");
        card.classList.add("card");
        card.appendChild(flightNumber);
        card.appendChild(date);
        card.appendChild(name);
        card.appendChild(status);
        card.appendChild(success);
        card.appendChild(button);
        document.querySelector(".cards").appendChild(card);
        button.addEventListener("click", e => {
            let rocketID = document.createElement("p");
            rocketID.innerHTML = `Rocket ID: ${el.rocket.rocket_id}`;
            let rocketName = document.createElement("p");
            rocketName.innerHTML = `Rocket name: ${el.rocket.rocket_name}`;
            let details = document.createElement("p");
            details.innerHTML = `Details: ${el.details}`;
            let siteName = document.createElement("p");
            siteName.innerHTML = `Launch site name: ${el.launch_site.site_name}`;
            let missionPatch = document.createElement("img");
            missionPatch.setAttribute("src", el.links.mission_patch ? el.links.mission_patch : "noimage.jpg");
            let closeButton = document.createElement("button");
            closeButton.innerHTML = `X`;
            let moreInfo = document.createElement("div");
            moreInfo.appendChild(rocketID);
            moreInfo.appendChild(rocketName);
            moreInfo.appendChild(details);
            moreInfo.appendChild(siteName);
            moreInfo.appendChild(missionPatch);
            let popupWindow = document.querySelector(".popup");
            popupWindow.appendChild(moreInfo);
            popupWindow.appendChild(closeButton);
            let disactiv = document.querySelector(".disactivate");
            disactiv.classList.remove("none");
            closeButton.addEventListener("click", e => {
                disactiv.classList.add("none");
                while (popupWindow.firstChild) {
                    popupWindow.removeChild(popupWindow.firstChild);
                }
            })
        })
    })
}
document.querySelector(".showResults").addEventListener("click", e=> {
    getResults();
})
function getResults() {
    displayedLaunches = [];
    let emptyWrapper = document.querySelector(".cards");
    while (emptyWrapper.firstChild) {
        emptyWrapper.removeChild(emptyWrapper.firstChild);
    }
    let selected = document.querySelector("select")
    if (selected.selectedIndex == 0) {
        getAllLaunches();
    } else if (selected.selectedIndex == 1) {
        fetch("https://api.spacexdata.com/v3/launches/upcoming?limit=10").then(resp => {
            return resp.json();
        }).then(json => {
            json.forEach(el => {
                displayedLaunches.push(el);
            });
          display();
        })
    } else if (selected.selectedIndex == 2) {
        allLaunches.forEach(el => {
            if (el.launch_success) {
                displayedLaunches.push(el);
            } else return '';
            display();
        });
    } else if (selected.selectedIndex == 3) {
        allLaunches.forEach(el => {
            if (!el.launch_success) {
                displayedLaunches.push(el);
            } else return '';
            display();
        }); 
    };
    
}