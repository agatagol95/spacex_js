# SpaceX Launches App 
This application uses API in order to display previous and upcoming launches of SpaceX, together with their details. I used HTML5, JavaScript and preprocessor SASS to write this app. 

## Tips 
I am using fetch API in this project, which is not supported by Internet Explorer, so I strongly recommend using any other browser. 

## Installing 
To install this app, clone this repo, go to root folder and run:
`npm install`.
When npm finish installing dependencies, just run
`npx http-server`
to start live-reload server locally.